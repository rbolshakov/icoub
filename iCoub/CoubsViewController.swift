//
//  CoubsViewController.swift
//  iCoub
//
//  Created by Roman Bolshakov on R 1/11/23.
//  Copyright © Reiwa 1 Roman Bolshakov. All rights reserved.
//

import UIKit
import SDWebImage

class CoubsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var table: UITableView!
    var refreshControl = UIRefreshControl()
    
    var coubs: [Coub] = []
    var selectedCoub: Coub! = nil
    var page = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        table.addSubview(refreshControl)
        refresh(sender: self)
    }
    
    //MARK: table methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return coubs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CoubPreviewCell
        let coub = coubs[indexPath.row]
        cell.titleLabel.text = coub.title
        cell.viewsLabel.text = "\(coub.viewsCount!)"
        cell.likesLabel!.text = "\(coub.likesCount!)"
        cell.previewImageView.sd_setImage(with: URL(string: coub.previewUrl!), completed: nil)
        if (coubs.count > 0 && coub === coubs.last!) { //Last visible element of table
            page+=1
            CoubNetworkManager.sharedInstance.getCoubs(page: page) { coubs in
                self.coubs += coubs
                self.table.reloadData()
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCoub = coubs[indexPath.row]
        performSegue(withIdentifier: "showNext", sender: self)
    }

    @objc func refresh(sender:AnyObject) {
        page = 1
        CoubNetworkManager.sharedInstance.getCoubs(page: page) { coubs in
            self.coubs = coubs
            self.refreshControl.endRefreshing()
            self.table.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let coubScreen = segue.destination as! CoubViewController
        coubScreen.coub = selectedCoub
    }
}
