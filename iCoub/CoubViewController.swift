//
//  CoubViewController.swift
//  iCoub
//
//  Created by Roman Bolshakov on R 1/11/23.
//  Copyright © Reiwa 1 Roman Bolshakov. All rights reserved.
//

import UIKit
import SDWebImage
import Photos

class CoubViewController: UIViewController, UITableViewDataSource {
    var coub: Coub!
    var similarCoubs: [Coub] = []
    var loading = false
    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CoubNetworkManager.sharedInstance.getCoubs(similarTo: coub, count: 10, completionHandler: { coubs in
            self.similarCoubs = coubs
            self.table.reloadData()
        })
    }
    
    //MARK: Table methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return similarCoubs.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "coub_cell", for: indexPath) as! CoubCell
            cell.titleLabel.text = coub.title
            cell.tagsLabel.text = coub.tagString
            cell.viewsLabel.text = "\(coub.viewsCount!)"
            cell.likesLabel.text = "\(coub.likesCount!)"
            cell.previewImageView.sd_setImage(with: URL(string: coub.previewUrl!), completed: nil)
            if loading {
                cell.showLoader()
            } else {
                cell.hideLoader()
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CoubPreviewCell
            let coub = similarCoubs[indexPath.row]
            cell.titleLabel.text = coub.title
            cell.viewsLabel.text = "\(coub.viewsCount!)"
            cell.previewImageView.sd_setImage(with: URL(string: coub.pictureUrl), completed: nil)
            return cell
        }
    }
    
    func tableView( _ tableView : UITableView,  titleForHeaderInSection section: Int)->String?
    {
        switch(section)
        {
        case 1:
            return "Similar coubs"
        default:
            return nil
        }
    }
    
    //MARK: other
    
    @IBAction func downloadVideo(_ sender: Any) {
        if let url = URL(string: coub!.downloadUrl!) {
            loading = true
            table.reloadData()
            VideoFetcher().downloadAndSave(videoUrl: url) { success in
                self.loading = false
                DispatchQueue.main.async {
                    self.table.reloadData()
                }
            }
        }
    }
    
    deinit {
        debugPrint("deinit")
    }
}
