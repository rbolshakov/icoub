//
//  CoubPreviewCell.swift
//  iCoub
//
//  Created by Roman Bolshakov on R 1/11/23.
//  Copyright © Reiwa 1 Roman Bolshakov. All rights reserved.
//

import UIKit

class CoubPreviewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var likesLabel: UILabel?
    @IBOutlet weak var viewsLabel: UILabel!
    @IBOutlet weak var previewImageView: UIImageView!
}
