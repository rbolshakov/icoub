//
//  Coub.swift
//  iCoub
//
//  Created by Roman Bolshakov on R 1/11/23.
//  Copyright © Reiwa 1 Roman Bolshakov. All rights reserved.
//

import Foundation
class Coub {
    var title: String!
    var likesCount: Int?
    var viewsCount: Int!
    var previewUrl: String?
    var pictureUrl: String!
    var downloadUrl: String?
    var permalink: String?
    var tags: [String] = []
    var tagString: String {
        var result = ""
        for tag in tags {
            result += "#\(tag) "
        }
        return result
    }
}
