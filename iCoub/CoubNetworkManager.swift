//
//  CoubNetworkManager.swift
//  iCoub
//
//  Created by Roman Bolshakov on R 1/11/23.
//  Copyright © Reiwa 1 Roman Bolshakov. All rights reserved.
//

import Foundation
import Alamofire

class CoubNetworkManager {
    static let sharedInstance = CoubNetworkManager()
    
    let coubs_url = "https://coub.com/api/v2/timeline/hot"
    let similar_coubs_url = "https://coub.com/api/v2/coubs/%@/category_suggestions"
    
    private init() {
        // Private initialization to ensure just one instance is created.
    }
    
    func getCoubs(page: Int, completionHandler: @escaping ([Coub]) -> Void) {
        var coubs: [Coub] = []
        AF.request(coubs_url,
                   method: .get,
                   parameters: ["page": page, "per_page": 10],
                   encoder: URLEncodedFormParameterEncoder.default).responseJSON { response in
                        if let responseDictionary = response.value as? [String: AnyObject] {
                            if let coubsArray = responseDictionary["coubs"] as? [AnyObject] {
                                for coubElement in coubsArray {
                                    let coub = Coub()
                                    coub.title = coubElement["title"] as? String
                                    coub.permalink = coubElement["permalink"] as? String
                                    coub.viewsCount = coubElement["views_count"] as? Int
                                    coub.likesCount = coubElement["likes_count"] as? Int
                                    coub.previewUrl = coubElement["small_picture"] as? String
                                    coub.pictureUrl = coubElement["picture"] as? String
                                    if let tagsArray = coubElement["tags"] as? [AnyObject] {
                                        for tagElement in tagsArray {
                                            if let tag = tagElement["title"] as? String {
                                                coub.tags.append(tag)
                                            }
                                        }
                                    }
                                    if let fileDictionary = coubElement["file_versions"] as? [String: AnyObject] {
                                        if let shareDictionary = fileDictionary["share"] as? [String: AnyObject] {
                                            coub.downloadUrl = shareDictionary["default"] as? String
                                        }
                                    }
                                    coubs.append(coub)
                                }
                            }
                        }
                    debugPrint("Response: \(coubs)")
                    completionHandler(coubs)
                   }
    }
    
    func getCoubs(similarTo coub:Coub, count:Int, completionHandler: @escaping ([Coub]) -> Void) {
        var coubs: [Coub] = []
        let string_url = String(format: similar_coubs_url, coub.permalink!)
        AF.request(string_url,
                   method: .get,
                   parameters: ["count": count],
                   encoder: URLEncodedFormParameterEncoder.default).responseJSON { response in
                        if let responseDictionary = response.value as? [String: AnyObject] {
                            if let coubsArray = responseDictionary["coubs"] as? [AnyObject] {
                                for coubElement in coubsArray {
                                    let coub = Coub()
                                    coub.title = coubElement["title"] as? String
                                    coub.viewsCount = coubElement["views_count"] as? Int
                                    coub.pictureUrl = coubElement["image_url"] as? String
                                    coubs.append(coub)
                                }
                            }
                        }
                    debugPrint("Response: \(coubs)")
                    completionHandler(coubs)
                   }
    }
}
