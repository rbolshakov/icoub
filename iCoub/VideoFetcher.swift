//
//  VideoFetcher.swift
//  iCoub
//
//  Created by Roman Bolshakov on R 1/11/24.
//  Copyright © Reiwa 1 Roman Bolshakov. All rights reserved.
//

import UIKit
import Alamofire
import Photos

/// Download and save video to camera roll
class VideoFetcher {
  func downloadAndSave(videoUrl: URL, completion: @escaping (Bool) -> Void) {
    let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)
    AF.download(videoUrl, to: destination).response { response in
        debugPrint(response)
        if response.error == nil, let fileURL = response.fileURL {
            self.save(videoFileUrl: fileURL, completion: completion)
        }
    }
  }

  private func save(videoFileUrl: URL, completion: @escaping (Bool) -> Void) {
    PHPhotoLibrary.shared().performChanges({
      PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoFileUrl)
    }, completionHandler: { succeeded, error in
      guard error == nil, succeeded else {
        completion(false)
        return
      }
     
      try? FileManager.default.removeItem(at: videoFileUrl)
        
      completion(true)
    })
  }
}
